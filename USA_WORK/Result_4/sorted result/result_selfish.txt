set terminal png size 1300,900 enhanced font "Helvetica,20"
set output 'result_selfish.png'
set key at 0.9,0.7 spacing 1.2
set xlabel offset -2,0 font "Times Roman, 25"
set ylabel offset 1.4,0 font "Times Roman, 23"
set tics font "Times Roman, 25"
set key font "Times Roman, 25"
set style line 1 lc rgb "blue" lt 1 lw 2 pt 7 ps 1.5   # --- blue
set style line 2 lc rgb "red" lt 1 lw 2 pt 5 ps 1.5   # --- red
set style line 3 lc rgb "black" lt 1 lw 2 pt 3 ps 1.5   # --- red
set title '' font "Times Roman, 25"
set xlabel 'Object Sensitivity'
set ylabel 'Risk Score'
set key left
plot 'A1_selfish.txt' using 5:6 with linespoints ls 1 title 'View','A2_selfish.txt' using 5:6 with linespoints ls 3 title 'Edit','A3_selfish.txt' using 5:6 with linespoints ls 2 title 'Execute'