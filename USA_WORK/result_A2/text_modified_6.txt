############################################# Incentives

set terminal postscript eps enhanced color
set boxwidth 0.95 absolute
set key at 0.66,0.85 spacing 1.7
set style line 1 lc rgb "blue" lt 1 lw 2 pt 7 ps 1   # --- blue
set style line 2 lc rgb "red" lt 1 lw 2 pt 5 ps 1   # --- red
set style line 3 lc rgb "black" lt 1 lw 2 pt 3 ps 1   # --- ed
#set style fill   solid 0.50 border lt -1
#unset key
#set pointsize 0.5
#set minussign
set style data boxplot
set ylabel "{/Times:Bold Risk Score"
set xlabel "{/Times:Bold Object Sensitivity}"
set xtics border in scale 0,0 nomirror norotate  autojustify
set xtics  norangelimit 
set ytics border in scale 1,0.5 nomirror norotate  autojustify
set ytics font "Times-Roman,30" 
set xtics font "Times-Roman,30"
set xlabel font "Times-Roman,35"
set ylabel font "Times-Roman,35" 
set key font "Times-Roman, 30"
set lmargin at screen 0.17
set bmargin 8
set xlabel offset 0,-3
set ylabel offset -5,2
set xtics offset -1.5,-0.5
#set yrange [ 0.00000 : 100.000 ] noreverse nowriteback
#x = 0.0
## Last datafile plotted: "silver.dat"
set output '6.eps'
plot 'A2_honest.txt' using 5:6 with linespoints ls 1 title '{/Times:Bold Honest','A2_selfish.txt' using 5:6 with linespoints ls 3 title '{/Times:Bold Selfish}','A2_malicious.txt' using 5:6 with linespoints ls 2 title '{/Times:Bold Malicious}



