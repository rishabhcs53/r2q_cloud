import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.lang.*;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.io.*;



/**
 * Threat Estimation Code
 *
 * @author  Rishabh Singhal (iitpacin@gmail.com)
 * @version 1.0
 * @since   2017-04-01
 */

/*
    TODO:
    1. Put Line 248 - 316 in different function
    2. Change function name line 510
    3.
*/

class Threat_Estimation_Runnable{

	 static String user_id;
	 static String object_id;
     static String access_id;
     static String requested_domain_id;
	 static double SSL=0;
	 static double normalised_SSL=0;
	 static double normalised_object_sensitivity=0;
	 static double coffcient_object_sensitivity=0; 
	 static double coffcient_ssl=0;
	 static double belief_mass=0;
	 static double reputation_score=0;
	 static double N_feedback=0;
	 static double asset_value=0;
	 static double expected_threat=0;
	 static double risk_score=0;
     static double normalised_expected_threat=0;
     static int count=0;
     static File file;
     static StringBuilder fileContent;
     static FileReader fileReader;
     static BufferedReader bufferedReader;
     static FileWriter fstreamWrite;
     static  BufferedWriter out;
     static double[] parameter;
     static  FileInputStream fstream;
     public static String input;
     static double service_benefit=0;

     static FileOutputStream fos = null;

    



      void write_in_file(String reuqest,double normalised_object_sensitivity,double risk_score,int time)
      {
          /*
          * Code used for plotting result. Write to a file for plotting result
          * */
        try
        {
          int o1=time+1;
          String file_name_1="/home/rishabh/Desktop/Cloud_security_journal_code/USA_WORK/"+"sensitivity_risk_A3_malicious_"+o1+"_50.txt";
          file=new File(file_name_1);
                  fos = new FileOutputStream(file,true);
                  if (!file.exists()) {
                                             file.createNewFile();
                                         }

                  String hh=reuqest+" "+normalised_object_sensitivity+" "+risk_score;
                  byte[] bytesArray = hh.getBytes();

         fos.write(bytesArray);
          String gg="\n";
          byte[] bytesArray1 = gg.getBytes();

           fos.write(bytesArray1);

           fos.flush();


        }

        catch(Exception e)
        {

        }
      }

      void change_interaction_file(String request, String collabration)
      {
          /*
          *  Increase the Interaction of a requester based on its intial threat sensitivity. Users are of three Type
             Honest, Selfish, Malicious
          * */
        String[] req=request.split(",");
        String domain_id=req[0];
        String user_id=req[1];
        String[] jreq=collabration.split("\\s");
        double rate_of_bad_req=Double.parseDouble(jreq[3]);
        try
        {
            String file_name="";
            file_name="/home/rishabh/Desktop/Cloud_security_journal_code/USA_WORK/"+domain_id+"_"+"interaction.txt";
            FileInputStream fstream = new FileInputStream(file_name);
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            String line;
            StringBuilder fileContent = new StringBuilder();
            while ((line = br.readLine()) != null) {
              String[] gg1=line.split("\\s");
              if(gg1[0].equals(user_id))
        {
            Random rand=new Random();
          double L1=0;
          double H1=1;
          double R1 =(H1-L1)*rand.nextDouble() + L1;
          if(R1>rate_of_bad_req)
          {
              int l1=Integer.parseInt(gg1[1]);
              l1=l1+1;
              String u1="";
              u1=u1+l1;
              gg1[1]=u1;
          }

            String newline=gg1[0]+" "+gg1[1];
            //System.out.println(newline);
            fileContent.append(newline);
            fileContent.append("\n");
        }
        else
        {
             fileContent.append(line);
             fileContent.append("\n");

            }
        }

            FileWriter fstreamWrite = new FileWriter(file_name);
            BufferedWriter out = new BufferedWriter(fstreamWrite);
            out.write(fileContent.toString());
            out.close();
            //Close the input stream
            fstream.close();


        }
        catch(Exception e)
        {

        }

      }

      void threat_estimation_start(String request,String collabration,int simulation_time_cloud_project)
      {
          /*
          * This is function to estimate Risk Score of a particular request
          * @param request The request contain information about Requester Domain ID, Requester ID, SSL,
                            requested Domain ID, Object ID, access Mode
          * @param collabration  This is a string which is a combination of request, ,object sensitivity level, 
                                request risk category intially (at simulation_time = 0), risk score of the requester 
                                 intially (at simulation_time = 0) 
          * @param simulation_time_cloud_project  Simulation Time
          * @return void Calculate risk score of the request
          * */

          SSL=0;
          normalised_SSL=0;
          normalised_object_sensitivity=0;
          coffcient_object_sensitivity=0;
          coffcient_ssl=0;
          belief_mass=0;
          reputation_score=0;
          N_feedback=0;
          asset_value=0;
          expected_threat=0;
          risk_score=0;
          fos=null;
          service_benefit=0;

          normalised_expected_threat=0;

          double L1=0,H1=0,R1=0;

          Random r1 = new Random();


          parameter=new double[7];
          int z=0;

            try
            {
                    file = new File("/home/rishabh/Desktop/Cloud_security_journal_code/USA_WORK/parameters.txt");
                    FileReader fileReader = new FileReader(file);
                    BufferedReader bufferedReader = new BufferedReader(fileReader);
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                                String[] gg1=line.split("\\s");
                                L1=Double.parseDouble(gg1[1]);
                                H1=Double.parseDouble(gg1[2]);
                                R1 = (H1-L1)*r1.nextDouble() + L1;
                                parameter[z]=R1;
                                z++;


                    }
            }

            catch(Exception e)
            {

            }




		
        Scanner s1=new Scanner(System.in);
        String collabration_request=request;
        //System.out.println("**************Sending Request to Access Request Handler**************************");
        access_request_Handler(collabration_request);
        //authenticate_request(object_id,user_id,access_id);
        int authenticate_check=1;

        if(authenticate_check==1)
        {

       
        double tao1= gompertz(user_id,requested_domain_id);

        uncertinity_measure(reputation_score);

        normalised_SSL=(SSL-1)/9;

        object_repository(collabration);

        richard_logistic(collabration,access_id);

        weighted_linear_regression(coffcient_object_sensitivity,normalised_object_sensitivity,coffcient_ssl,normalised_SSL);

        normalised_threat_function(expected_threat);

        risk(normalised_expected_threat);


        if(access_id.equals("A1"))
        service_benefit=normalised_object_sensitivity*(0.004);
        else
        service_benefit=normalised_object_sensitivity*(0.05);


        // Generate Simulation result Code
        /*if(simulation_time_cloud_project==199)
        System.out.println(request+" "+risk_score+" "+service_benefit);

        if(simulation_time_cloud_project==199)
        System.out.println(request+" "+risk_score);





            String request_type="";
            String simulation_request_type="";
            String[] yy1=collabration.split("\\s");
            request_type=yy1[1];

            if((risk_score>0.8)&&(risk_score<=1))
            {
              simulation_request_type="H";
            }
            else if ((risk_score>0.5)&&(risk_score<=0.8)) {

              simulation_request_type="M";

            }
            else
            {
              simulation_request_type="L";
            }


          System.out.println(request+" "+request_type+" "+simulation_request_type);

        if(simulation_time_cloud_project==9)
        {
          write_in_file(collabration,normalised_object_sensitivity,risk_score,simulation_time_cloud_project);
        }
        else if(simulation_time_cloud_project==19)
        {
          write_in_file(collabration,normalised_object_sensitivity,risk_score,simulation_time_cloud_project);
        }
        else if(simulation_time_cloud_project==29)
        {
          write_in_file(collabration,normalised_object_sensitivity,risk_score,simulation_time_cloud_project);
        }
        else if(simulation_time_cloud_project==39)
        {
          write_in_file(collabration,normalised_object_sensitivity,risk_score,simulation_time_cloud_project);
        }

        if(simulation_time_cloud_project==49)
        {
          write_in_file(collabration,normalised_object_sensitivity,risk_score,simulation_time_cloud_project);
        }

        else if(simulation_time_cloud_project==59)
        {
          write_in_file(collabration,normalised_object_sensitivity,risk_score,simulation_time_cloud_project);
        }

        else if(simulation_time_cloud_project==299)
        {
          write_in_file(collabration,normalised_object_sensitivity,risk_score,simulation_time_cloud_project);
        }




        System.out.println(request+" "+acutal_type+" "+simulation_type);
        */
        }

        else
        {
          System.out.println(" REQUEST :"+ collabration_request+ " You are not authenticated user ");
          System.exit(0);
        }

}



       static void access_request_Handler(String collabration)
       {
            /*
             * This is function to extracts the identity of the requester,
               its security level, and the information about the requested object.
             * @param collabration  This is a string which is a combination of request, ,object sensitivity level,
                                request risk category intially (at simulation_time = 0), risk score of the requester
                                 intially (at simulation_time = 0)
             * @return void Extract information of requester, its ssl and requested object and access_id of object
            */

            String[] request=collabration.split(",");


            user_id=request[1];
            double j1=Double.parseDouble(request[2]);
            SSL=j1;
            requested_domain_id=request[3];

            object_id=request[4];
            access_id=request[5];
            System.out.println(access_id);


        }

       static double gompertz(String U_ID,String requested_domain_id)
       {

             /*
              * This function is used to calculate use inverse Gompertz function to model the requester’s reputation
              * @param U_ID User ID of the requester
              * @param requested_domain_id Domain ID of the requested system
              * @return return Value of tao (Number of Interaction) and calculate reputation score using formula (1) in paper
              */


              double tao=0;

              try
              {
                String file_name="";

                String kkw=U_ID.substring(0,2);
                file_name="/home/rishabh/Desktop/Cloud_security_journal_code/USA_WORK/"+kkw+"_"+"interaction.txt";
                file = new File(file_name);
                fileReader = new FileReader(file);
                bufferedReader = new BufferedReader(fileReader);
                String line;
                tao=0;
                while ((line = bufferedReader.readLine()) != null) {

                    String[] gg1=line.split("\\s");
                    if(gg1[0].equals(U_ID))
                    {

                      int k6=Integer.parseInt(gg1[1]);
                      tao=k6;
                      break;

                    }

              }

               System.out.print(tao+"is the score");
               double A=parameter[0];


               double B=parameter[1];


               double C=parameter[2];


               double tao1=tao;

               double r1=C*tao1;
               r1=(-1)*(r1);

               double m1= B*Math.exp(r1);
               m1=(-1)*m1;

               reputation_score= (1-(A* Math.exp(m1)));

             }
             catch (Exception e)
             {
               System.out.println("error in gompertz");
             }
            return tao;

    }


       static void uncertinity_measure(double reputation)
       {
           /*
            * This function calculate the uncertainty measure whether the requester will misuse the access
              , given his/her usage history
            * @param reputation Reputation Score of the requester
            * @return void Calculate the uncertainty measure using formula 3 in paper
            */

        if(reputation==0)
        {
            coffcient_ssl=1;
        }
        else
        {

             double lamda=parameter[3];
             double r1=lamda/reputation;
             r1=-1*r1;
             double m1= Math.exp(r1);
             coffcient_ssl=1-m1;

        }
    }


       static void object_repository(String collabration)
       {
           /*
            * This function calculate object senitivity. We normalize object sensitivity based on sensitivity Type.
               Four Types = ["Top_Secret", "Secret", "Confidential", "Unclassified"]
            * @param collabration This is a string which is a combination of request, ,object sensitivity level,
                                  request risk category intially (at simulation_time = 0), risk score of the requester
                                  intially (at simulation_time = 0)
            * @return void Calculate the normalised_object_sensitivity
            */

            String sensitivity="";
            double L1=0,H1=0,R1=0;


            String[] jj=collabration.split("\\s");
            sensitivity=jj[2];


             Random r1 = new Random();

            System.out.println("Following is object id "+O_ID+" and sensitivity "+sensitivity);
            if(sensitivity.equals("Top_Secret"))
            {
                L1=.901;
                H1=1;
                R1 = (H1-L1)*r1.nextDouble() + L1;


            }
            else if(sensitivity.equals("Secret"))
            {
                L1=.751;
                H1=.9;
                R1 = (H1-L1)*r1.nextDouble() + L1;

            }
            else if(sensitivity.equals("Confidential"))
            {
                L1=.601;
                H1=.75;
                R1 = (H1-L1)*r1.nextDouble() + L1;

            }
            else
            {
                  L1=.5;
                  H1=.6;
                  R1 = (H1-L1)*r1.nextDouble() + L1;

            }

        normalised_object_sensitivity=R1;

    }


        static void richard_logistic(String collabration,String access_id)
       {

           /*
            * This function is used for calculation of Expected Utility.
               Four Types = ["Top_Secret", "Secret", "Confidential", "Unclassified"]
            * @param collabration This is a string which is a combination of request, ,object sensitivity level,
                                  request risk category intially (at simulation_time = 0), risk score of the requester
                                  intially (at simulation_time = 0)
            * @param  access_id Calculate delta based on access id. Paper provides description about impact of access
                                modes in terms of three security tenets (confidentiality (C), integrity (I), and
                                availability (A)) when subjected to different object types
            * @return void Calculate the coffcient_object_sensitivity using formula (5) in paper
            */


        String sensitivity="";
        String[] jj=collabration.split("\\s");
        sensitivity=jj[2];
        double delta=0;
        double C=0,I=0,A=0;
        double p_acces_id=0;
       //Sensitive

          if((sensitivity.equals("Top_Secret"))||(sensitivity.equals("Secret"))||(sensitivity.equals("Confidential")))
          {
            if(access_id.equals("A1")) //View
            {
              C=1;
              I=0;
              A=0;
              p_acces_id=0.3455596890931343;
              delta=p_acces_id*(C+I+A);
            }
            else if(access_id.equals("A2")) // EDIT
            {
              C=0;
              I=1;
              A=1;
              p_acces_id=0.31335862085734434;
              delta=p_acces_id*(C+I+A);
            }
            else //EXECUTE
            {
              C=0;
              I=1;
              A=1;
              p_acces_id=0.3410816900495214;
              delta=p_acces_id*(C+I+A);

            }


          }
          else //Non sensitive
          {
            if(access_id.equals("A1")) //View
            {
              C=0;
              I=0;
              A=1;
              p_acces_id=0.3455596890931343;
              delta=p_acces_id*(C+I+A);
            }
            else if(access_id.equals("A2")) // EDIT
            {

              C=0;
              I=1;
              A=1;
              p_acces_id=0.31335862085734434;
              delta=p_acces_id*(C+I+A);
            }
            else //EXECUTE
            {
               C=0;
               I=1;
               A=1;
               p_acces_id=0.3410816900495214;
               delta=p_acces_id*(C+I+A);

            }


          }


        double v=parameter[4];

        v=(1/v);

        double m1=delta*v;
        m1=-1*m1;
        double r1=Math.exp(m1);

        coffcient_object_sensitivity= 1-r1;

    }

        static void weighted_linear_regression(double coffcient_object_sensitivity,double normalised_object_sensitivity,double coffcient_ssl,double normalised_SSL)
        {

        /*
            * This function is used for calculation of Expected Threat.
            * @param coffcient_object_sensitivity Calculated using function richard_logistic
            * @param normalised_object_sensitivity Calcuated using function object_repository
            * @param coffcient_ssl  Calculated using function uncertinity_measure
            * @param normalised_SSL Given in data
            * @return void Calculate the expected_threat using formula (2) in paper
            */

        expected_threat=(coffcient_object_sensitivity*normalised_object_sensitivity)+(coffcient_ssl*normalised_SSL);
        System.out.println("Value of expected threat " + expected_threat);
        }

        static void normalised_threat_function(double expected_threat)
        {
            /*
            *  Normalize the expected threat
            * @param expected_threat Calculated using function weighted_linear_regression
            * @return void Calculate the normalised_expected_threat
            * */
            normalised_expected_threat=(expected_threat)/2;
        }


        static void risk(double threat)
        {

            /*
             * This function is used for calculation risk score between 0-1.
             * @param threat Calculated using function normalised_threat_function
             * @return void Calculate the risk_score using function (6) in paper
             */

        double L1=0,H1=0,R1=0;
        Random r1 = new Random();
            if(threat>=(0.5))
            {
                   System.out.println("Randomly choose phi");
                   R1 =parameter[5];
                   double phi=R1;
                   risk_score=Math.pow(threat,phi);

            }
            else
            {
                System.out.println("Choose gamma gretaer than 1");
                double gam=parameter[6];
                double t1=Math.pow(threat,gam);
                risk_score=t1;

            }
    }
}


public class Threat_Estimation4 {

      static int final_number_lines=0;
      static String[] main_collabration;
      static String[] collabration_request_array;
      static File file;
      static int total_request=0;
      static Set<Integer> set = new HashSet<>();
 
  

   public static void main(String args[]) {

       /*
       *  Main Driver Code that Run RC2 Simulator
       * */

     Scanner s1=new Scanner(System.in);
     int simulation_time_cloud_project=0;
     System.out.println("RC2 Simulation Starts................");

     simulation_time_cloud_project=50;

     long startTime = System.currentTimeMillis();

     int max_client=50;
     main_collabration=new String[max_client];
     collabration_request_array=new String[max_client];


     /*
     Using Collabration Dataset
 try{

      int j1=0;
      int o1=0;
      while(set.size()<201)
      {
        j1=set.size();
        Random rand1=new Random();
        int l1=1;
        int h1=246762;
        int r1=(rand1.nextInt(h1-l1+1))+l1;

        set.add(r1);
        
        //System.out.println(j1+" "+set.size()+" "+r1);
        if((j1)!=(set.size()))
        {
          try
          {
              file=new File("/home/rishabh/Desktop/Cloud_security_journal_code/USA_WORK/collabration_request_data_set.txt");
              FileReader fileReader = new FileReader(file);
              BufferedReader bufferedReader = new BufferedReader(fileReader);
              String line;
              int p1=1;
          while ((line = bufferedReader.readLine()) != null) {

            if(r1==p1)
            {
              collabration_request_array[o1]=line;
              o1++;
              break;
            }
            p1++;

          }

          }
          catch(Exception e)
          {

          }
        }
        

      }

    
  }
  catch(Exception e)
  {

  }
  */


  
  try{

      int j1=0;
      int o1=0;

          try
          {
              file=new File("/home/rishabh/Desktop/Cloud_security_journal_code/USA_WORK/file_A3_malicious.txt");
              FileReader fileReader = new FileReader(file);
              BufferedReader bufferedReader = new BufferedReader(fileReader);
              String line;
          while ((line = bufferedReader.readLine()) != null) {

           
              collabration_request_array[o1]=line;
              o1++;
             
          

          }

          }
          catch(Exception e)
          {

          }
    
  }
  catch(Exception e)
  {

  }





  /////////////////////////////////////////////////////////////



     for(int k1=0;k1<simulation_time_cloud_project;k1++)
     {
  
   
        try{
              for(int i=0;i<max_client;i++)
                {

                  String[] gg=collabration_request_array[i].split("\\s");
                  main_collabration[i]=gg[0];
                  System.out.println("Request is " + main_collabration[i]);


                  Threat_Estimation_Runnable t=new Threat_Estimation_Runnable();
                  t.threat_estimation_start(main_collabration[i],collabration_request_array[i],k1);
                  t.change_interaction_file(main_collabration[i],collabration_request_array[i]);


                }
            }
        catch(Exception e)
        {

        }

        }


      long stopTime = System.currentTimeMillis();

      long elapsedTime = stopTime - startTime;
      System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+startTime+" "+stopTime+" "+elapsedTime);

    /*try
        {

        String kk="/home/rishabh/Desktop/Cloud_security_journal_code/USA_WORK/"+total_request+".txt";
          File file1=new File(kk);
               FileOutputStream fos1 = new FileOutputStream(file1,true);
                          if (!file1.exists()) {
                                                     file1.createNewFile();
                                                 }

                          String hh="";
             hh=hh+elapsedTime;
                          byte[] bytesArray = hh.getBytes();

                 fos1.write(bytesArray);
                  String gg="\n";
                  byte[] bytesArray1 = gg.getBytes();

                   fos1.write(bytesArray1);

                   fos1.flush();
        }

        catch(Exception e)
        {


        }*/
  
   }   
}


