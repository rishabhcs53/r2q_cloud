import java.io.*;
import java.util.*;

public class SORT {

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new FileReader("/home/rishabh/Desktop/Cloud_security_journal_code/USA_WORK/sensitivity_risk_A3_malicious_50_50.txt"));
        Map<Double, String> map=new TreeMap<Double, String>();
        String line="";
        while((line=reader.readLine())!=null){
            map.put(getField(line),line);
        }
        reader.close();
        FileWriter writer = new FileWriter("/home/rishabh/Desktop/Cloud_security_journal_code/USA_WORK/A3_malicious.txt");
        for(String val : map.values()){
            writer.write(val);  
            writer.write('\n');
        }
        writer.close();
    }

    private static Double getField(String line) {
        return Double.parseDouble(line.split("\\s")[4]);//extract value you want to sort on
    }
}