############################################# Incentives

set terminal postscript eps enhanced color
set boxwidth 0.95 absolute
set output 'result_3.eps'
set key right spacing 1.6
set xlabel offset -2,-1 font "Times Roman, 25"
set ylabel offset 1.5,0 font "Times Roman, 25"
red = "#FF0000"; green = "#00FF00"; blue = "#0000FF"; skyblue = "#87CEEB";
set yrange [0:90]
set style data histogram
set style histogram cluster gap 1
set style fill solid
set boxwidth 0.5
set xtics border in scale 0,0 nomirror norotate  autojustify
set xtics  norangelimit 
set ytics border in scale 1,0.5 nomirror norotate  autojustify
set ytics font "Times-Roman,30" 
set xtics font "Times-Roman,30"
set xlabel font "Times-Roman,35"
set ylabel font "Times-Roman,35" 
set ylabel "{/Times:Bold Number of Requests Selected}"
set xlabel "{/Times:Bold Simulation Time}"
set key font "Times-Roman, 30"
set xlabel offset 0,-3
set ylabel offset -5,-2
set xtics offset -1.5,-0.5
set title "" font "Times Roman, 25"
plot "re2.txt" using ($2):xtic(1) title '{/Times:Bold MORS}' linecolor rgb red, \
            '' using ($3) title '{/Times:Bold DRD}' linecolor rgb blue, \
   


