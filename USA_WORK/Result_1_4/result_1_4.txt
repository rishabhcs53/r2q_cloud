set terminal png size 1300,900 enhanced font "Helvetica,20"
set output 'result_1_4.png'
set key left
set key at 0.3,0.95 spacing 1.2
set xlabel offset -2,0 font "Times Roman, 25"
set ylabel offset 1.4,0 font "Times Roman, 23"
set tics font "Times Roman, 25"
set key font "Times Roman, 25"
set style line 1 lc rgb "blue" lt 1 lw 2 pt 7 ps 1.5   # --- blue
set style line 2 lc rgb "red" lt 1 lw 2 pt 5 ps 1.5   # --- red
set style line 3 lc rgb "black" lt 1 lw 2 pt 3 ps 1.5   # --- red
set title '' font "Times Roman, 25"
set xlabel 'Normalized Expected Threat'
set ylabel 'Risk Score'
plot 'result_1_4_phi_0.2_varphi_1.15.txt' using 1:2 with linespoints ls 1 title '{/Symbol F}=0.2, {/Symbol f}=1.15 ','result_1_4_phi_0.3_varphi_1.25.txt' using 1:2 with linespoints ls 3 title '{/Symbol F}=0.3, {/Symbol f}=1.25','result_1_4_phi_0.4_varphi_1.35.txt' using 1:2 with linespoints ls 2 title '{/Symbol F}=0.4, {/Symbol f}=1.35'